<?php
include_once 'Controller/SongController.php';
include_once 'Controller/AlbumController.php';
include_once 'Controller/UserController.php';


// defino la base url para la construccion de links con urls semánticas
define('BASE_URL', '//' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']) . '/');
define("LOGIN", 'http://'.$_SERVER["SERVER_NAME"].':'.$_SERVER["SERVER_PORT"].dirname($_SERVER["PHP_SELF"]).'/login');
define("LOGOUT", 'http://'.$_SERVER["SERVER_NAME"].':'.$_SERVER["SERVER_PORT"].dirname($_SERVER["PHP_SELF"]).'/logout');

// lee la acción
if (!empty($_GET['action'])) {
    $action = $_GET['action'];
} else {
    $action = 'home'; // acción por defecto si no envían
}

// parsea la accion Ej: suma/1/2 --> ['suma', 1, 2]
$params = explode('_', $action);
// determina que camino seguir según la acción
switch ($params[0]) {
    case 'home':
        $controller = new SongController();
        $controller->home();
        break;
    case 'songs':
        $controller = new SongController();
        if(isset($params[1])){
            $album = $params[1];
            $controller->showSongsByAlbum($album);
        }else{
            $controller->showSongs();
        }
        break;   
    case 'song':
        $controller = new SongController();
        $song = $params[1];
        $controller->showSong($song);
        break; 
    case 'albums':
        $controller = new AlbumController();        
        $controller->showAlbums();
        break;
    case 'album':
        $controller = new AlbumController();
        $album = $params[1];
        $controller->showAlbum($album);
        break;
    case 'login':
        $controller = new UserController();
        $controller->showLogin();
        break;
    case 'logout':
        $controller = new UserController();
        $controller->logout();
        break;
    case 'verify':
        $controller = new UserController();
        $controller->loginUser();
        break;
    case 'showInsert':
        if(isset($params[1])){
            if($params[1] == "album"){
                $controller = new AlbumController();
            }
            if($params[1] == "song"){
                $controller = new SongController();
            }
            $controller->showInsert();
        }
        break;
    case 'insert':
        $controller = new SongController();
        if(isset($params[1])){
            if($params[1] == "album"){
                $controller = new AlbumController();
            }
            if($params[1] == "song"){
                $controller = new SongController();
            }
            $controller->insert();
        }
        break;
    case 'delete':
        if(isset($params[1])){
            if($params[1] == "album"){
                $controller = new AlbumController();
            }
            if($params[1] == "song"){
                $controller = new SongController();
            }
            $controller->delete($params[2]);
        }
        break;
    case 'showUpdate':
        if(isset($params[1])){
            if($params[1] == "album"){
                $controller = new AlbumController();
            }
            if($params[1] == "song"){
                $controller = new SongController();
            }
            $controller->showUpdate($params[2]);
        }
        break;
    case 'update':
        if(isset($params[1])){
            if($params[1] == "album"){
                $controller = new AlbumController();
            }
            if($params[1] == "song"){
                $controller = new SongController();
            }
            $controller->update($params[2]);
        }
        break;
    case 'list':
        $controller = new SongController();
        $controller->showList();
        break;
    case 'register':
        $controller = new UserController();
        $controller->register();
        break;
    case 'registration':
        $controller = new UserController();
        $controller->registration();
        break;
    case 'users':
        $controller = new UserController();
        $controller->showUsers();
        break;
    case 'editUser':
        $controller = new UserController();
        $userId = $params[1];
        $controller->editUser($userId);
        break;
    case 'deleteUser':
        $controller = new UserController();
        $userId = $params[1];
        $controller->deleteUser($userId);
        break;
    case 'insertComment':
        $controller = new SongController();
        $controller->insertComment();
        break;
    default:
        $controller = new SongController();
        $controller->home();
        break;
}
