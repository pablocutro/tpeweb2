<?php
require_once './Model/AlbumModel.php';
require_once './View/AlbumView.php';
require_once './Controller/SongController.php';
require_once './Model/SongModel.php';

class AlbumController {

    private $songController;
    private $albumModel;
    private $albumView;
    private $songModel;

    public function __construct() {
        $this->albumModel = new AlbumModel();
        $this->albumView = new AlbumView();
        $this->songController = new SongController();
        $this->songModel = new SongModel();
    }

    private function checkLoggedIn(){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        
        if(!isset($_SESSION["EMAIL"])){
            header("Location: ". LOGIN);
            die();
        }else{
            if ( isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1000000)) { 
                header("Location: ". LOGOUT);
                die();
            }
        
            $_SESSION['LAST_ACTIVITY'] = time();
        }
    }

    function checkSession(){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        
        return(!isset($_SESSION["EMAIL"]));
    }
    
    function makeArray($data){
        $array = array();

        foreach($data as $item) {
            array_push($array, $item);
        }

        return $array;
    }

    function showAlbums() {
        $albums = $this->makeArray($this->albumModel->getAlbums());
        if($this->checkSession()){
            $session = "login";$sessionMessage = "Log In";
        }else{
            $session = "logout";$sessionMessage = "Log Out";
        }
        $this->albumView->renderAlbums($albums, "All Albums", "All Albums", $session, $sessionMessage);
    }

    function showAlbum($albumName) {
        $albums = $this->makeArray($this->albumModel->getAlbums());
        $album = $this->makeArray($this->albumModel->getAlbum($albumName));
        if($this->checkSession()){
            $session = "login";$sessionMessage = "Log In";
        }else{
            $session = "logout";$sessionMessage = "Log Out";
        }
        $this->albumView->renderAlbum($albums, $album, "Album - " . $albumName, $session, $sessionMessage);
    }

    function delete($album_id){
        $this->checkLoggedIn();
        // verifico campos obligatorios
        $songs = $this->makeArray($this->songModel->getSongsByAlbumId($album_id));
        $count = count($songs);
        if($count == 0){
            $id = $this->albumModel->delete($album_id);
            $this->songController->showList("Album correctly deleted");
        }else{
            $this->songController->showList('Album contains songs, therefore cannot be deleted');
            die(); 
        }
    }

    function showUpdate($album_id, $message = ""){
        $this->checkLoggedIn();
        $albums = $this->makeArray($this->albumModel->getAlbums());
        $album = ($this->makeArray($this->albumModel->getAlbumById($album_id)))[0];
        if($this->checkSession()){
            $session = "login";$sessionMessage = "Log In";
        }else{
            $session = "logout";$sessionMessage = "Log Out";
        }
        $this->albumView->showUpdate($albums, $album, "Management - Edit Album", $message, $session, $sessionMessage); 
    }

    function update($album_id){
        $this->checkLoggedIn();
        $name = $_POST['name'];
        $description = $_POST['description'];
        $year = $_POST['year'];

        // verifico campos obligatorios
        if (empty($album_id) || empty($name) || empty($description) || empty($year)) {
            $this->showUpdate($album_id, 'Mandatory Data Missing');
            die();
        }
        $id = $this->albumModel->update($album_id, $name, $description, $year);
        $this->showUpdate($album_id, "Album edited correctly");
    }

    function showInsert($message = ""){
        $this->checkLoggedIn();
        $albums = $this->makeArray($this->albumModel->getAlbums());
        if($this->checkSession()){
            $session = "login";$sessionMessage = "Log In";
        }else{
            $session = "logout";$sessionMessage = "Log Out";
        }
        $this->albumView->showInsert($albums, "Management - Insert Album", $message, $session, $sessionMessage); 
    }

    function insert(){
        $this->checkLoggedIn();
        $name = $_POST['name'];
        $description = $_POST['description'];
        $year = $_POST['year'];
        // verifico campos obligatorios
        if (empty($name) || empty($description) || empty($year)) {
            $this->showInsert('Mandatory Data Missing');
            die();
        }

        $id = $this->albumModel->insert($name, $description, $year);
        $this->showInsert("Album added correctly");
    }
    
}