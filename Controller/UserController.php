<?php

require_once "./View/UserView.php";
require_once "./Model/UserModel.php";

class UserController{

    private $view;
    private $model;

    function __construct(){
        $this->view = new UserView();
        $this->model = new UserModel();

    }

    public function showLogin($message="") {
        $this->view->showFormLogin($message);
    }

    function Logout(){
        session_start();
        session_destroy();
        header("Location: ".home);

    }

    private function makeArray($data){
        $array = array();

        foreach($data as $item) {
            array_push($array, $item);
        }
        return $array;
    }

    private function checkSession(){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        
        return(!isset($_SESSION["EMAIL"]));
    }

    private function checkRole(){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $role = $_SESSION["ADMIN"];
        if($role == 0){
            header("Location: ".home);
            die();
        }
    }

    public function loginUser() {
        $email = $_POST['email'];
        $password = $_POST['password'];

        // verifico campos obligatorios
        if (empty($email) || empty($password)) {
            $this->showLogin("Faltan campos obligatorios");
        }

        // obtengo el usuario
        $user = $this->model->getByEmail($email);

        // si el usuario existe, y las contraseñas coinciden
        if ($user && password_verify($password, $user->password)) {
            session_start();
            $_SESSION["ADMIN"]= $user->admin;
            $_SESSION["EMAIL"] = $user->email;
            $_SESSION["ID"] = $user->id;
            $_SESSION['LAST_ACTIVITY'] = time();
            header("Location: ".home);
        } else {
            $this->showLogin("Credenciales inválidas");
        }
    }

    public function register(){
        $this->checkLoggedIn();
        $this->view->showRegister("");
    }

    public function registration(){
        $email = $_POST['email'];
        $password = $_POST['password'];

        // verifico campos obligatorios
        if (empty($email) || empty($password)) {
            $this->view->showRegister("Faltan campos obligatorios");
            die();
        }

        // obtengo el usuario
        $user = $this->model->getByEmail($email);

        // si el usuario no existe, lo crea
        if (!$user) {
            $this->model->register($email,$password);
            header("Location: ".home);
        } else {
            $this->view->showRegister("Usuario existente");
            die();
        }
    }

    private function checkLoggedIn(){
        $this->checkSession();
        
        if(!isset($_SESSION["EMAIL"])){
            header("Location: ". LOGIN);
            die();
        }else{
            if ( isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1000000)) { 
                header("Location: ". LOGOUT);
                die();
            }
        
            $_SESSION['LAST_ACTIVITY'] = time();
        }
    }

    function showUsers ($message = "") {
        $this->checkLoggedIn();
        $this->checkRole();

        $users = $this->makeArray($this->model->getUsers());
        foreach ($users as $key => $user) {
            if ($user->id == $this->getUserId()) {
                unset($users[$key]);
                break;
            }
        }
        $this->view->showUsers($users, $message);
    }

    function deleteUser($userId){
        $this->checkLoggedIn();
        $role = $this->checkRole();
        
        $user= $this->model->getUserById($userId);
        if ($user){
            $this->model->deleteUser($userId);
            $this->showUsers("User deleted.");
        }
        else{
            $this->showUsers("User couldn't be deleted.");
        }
    }

    function editUser($userId){
        $this->checkLoggedIn();
        $this->checkRole();
            
        if (isset($_POST['role'])) {
            $role = $_POST['role']; // value 1 para admin, 0 para user.
            $user = $this->model->getUserById($userId);

            if ($user){
                $this->model->updateUserRole($userId, $role);
                $this->showUsers("User role updated.");
            }
            else{
                $this->showUsers("User role couldn't be updayed.");
            }
        }
    }

    private function getUserId () {
        if (isset($_SESSION['ID'])) {
            return $_SESSION['ID'];
        }
        return null;
    }
}


?>