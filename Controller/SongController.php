<?php
require_once './Model/SongModel.php';
require_once './View/SongView.php';
require_once './Model/AlbumModel.php';
require_once './Model/CommentModel.php';
require_once './View/AlbumView.php';

class SongController {

    private $songModel;
    private $songView;
    private $albumModel;
    private $albumView;

    public function __construct() {
        $this->songModel = new SongModel();
        $this->songView = new SongView();
        $this->albumModel = new AlbumModel();
        $this->commentModel = new CommentModel();
        $this->albumView = new AlbumView();
    }

    private function checkLoggedIn(){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        
        if(!isset($_SESSION["EMAIL"])){
            header("Location: ". LOGIN);
            die();
        }else{
            if ( isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1000000)) { 
                header("Location: ". LOGOUT);
                die();
            }
        
            $_SESSION['LAST_ACTIVITY'] = time();
        }
    }

    function checkSession(){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        
        return(!isset($_SESSION["EMAIL"]));
    }

    private function checkRole(){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
        $role = $_SESSION["ADMIN"];
        if($role == 0){
            header("Location: ".home);
            die();
        }
    }
    
    function makeArray($data){
        $array = array();

        foreach($data as $item) {
            array_push($array, $item);
        }

        return $array;
    }

    function Home(){
        $albums = $this->makeArray($this->albumModel->getAlbums());
        if($this->checkSession()){
            $session = "login";$sessionMessage = "Log In";
        }else{
            $session = "logout";$sessionMessage = "Log Out";
        }
        $this->songView->ShowHome($albums, "Home", $session, $sessionMessage);
    }

    function showSongsByAlbum($album) {
        $albums = $this->makeArray($this->albumModel->getAlbums());  
        $songs = $this->makeArray($this->songModel->getSongsByAlbum($album));
        if($this->checkSession()){
            $session = "login";$sessionMessage = "Log In";
        }else{
            $session = "logout";$sessionMessage = "Log Out";
        }
        $this->songView->renderSongs($songs, $albums, $album, "Songs - " . $album, $session, $sessionMessage);
    }

    function showSongs() {
        $albums = $this->makeArray($this->albumModel->getAlbums());
        $songs = $this->makeArray($this->songModel->getSongs());
        if($this->checkSession()){
            $session = "login";$sessionMessage = "Log In";
        }else{
            $session = "logout";$sessionMessage = "Log Out";
        }
        $this->songView->renderSongs($songs, $albums, "All Songs", "All Songs", $session, $sessionMessage);
    }

    function showSong($song_id) {
        $albums = $this->makeArray($this->albumModel->getAlbums());
        $song = ($this->makeArray($this->songModel->getSong($song_id)))[0];
        if($this->checkSession()){
            $session = "login";$sessionMessage = "Log In";
            $user = null;
            $comments = null;
        }else{
            $session = "logout";$sessionMessage = "Log Out";
            $user = $_SESSION['ID'];
            $comments = $this->makeArray($this->commentModel->getComments($user));
        }
        $this->songView->renderSong($albums, $song, "Song - " . $song->name, $session, $sessionMessage, $user, $comments);
    }

    function showInsert($message = ""){
        $this->checkLoggedIn();
        $albums = $this->makeArray($this->albumModel->getAlbums());
        $songs = $this->makeArray($this->songModel->getSongs());
        if($this->checkSession()){
            $session = "login";$sessionMessage = "Log In";
        }else{
            $session = "logout";$sessionMessage = "Log Out";
        }
        $this->songView->showInsert($albums, $songs, "Management - Insert Song", $message, $session, $sessionMessage); 
    }

    function insert(){
        $this->checkLoggedIn();
        $this->checkRole();
        $title = $_POST['title'];
        $duration = $_POST['duration'];
        $writer = $_POST['writer'];
        $album_id = $_POST['album'];

        // verifico campos obligatorios
        if (empty($title) || empty($duration) || empty($writer) || empty($album_id)) {
            $this->showInsert('Mandatory Data Missing');
            die();
        }
        $id = $this->songModel->insert($title, $duration, $writer, $album_id);
        $this->showInsert("Song addded correctly");
    }

    function delete($song_id){
        $this->checkLoggedIn();
        $this->checkRole();
        $id = $this->songModel->delete($song_id);
        $this->showList("Song correctly deleted");
    }

    function showUpdate($song_id, $message = ""){
        $this->checkLoggedIn();
        $this->checkRole();
        $albums = $this->makeArray($this->albumModel->getAlbums());
        $songs = $this->makeArray($this->songModel->getSongs());
        $song = ($this->songModel->getSong($song_id))[0];
        if($this->checkSession()){
            $session = "login";$sessionMessage = "Log In";
        }else{
            $session = "logout";$sessionMessage = "Log Out";
        }
        $this->songView->showUpdate($albums, $songs, $song, "Management - Edit Song", $message, $session, $sessionMessage); 
    }

    function update($song_id){
        $this->checkLoggedIn();
        $this->checkRole();
        $title = $_POST['title'];
        $duration = $_POST['duration'];
        $writer = $_POST['writer'];
        $album = $_POST['album'];

        // verifico campos obligatorios
        if (empty($song_id) || empty($title) || empty($duration) || empty($writer) || empty($album)) {
            $this->showUpdate($song_id, 'Mandatory Data Missing');
            die();
        }
        $id = $this->songModel->update($song_id, $title, $duration, $writer, $album);
        $this->showUpdate($song_id, "Song edited correctly");
    }

    function showList($message = ""){
        $this->checkLoggedIn();
        $this->checkRole();
        $albums = $this->makeArray($this->albumModel->getAlbums());
        $songs = $this->makeArray($this->songModel->getSongs());
        if($this->checkSession()){
            $session = "login";$sessionMessage = "Log In";
        }else{
            $session = "logout";$sessionMessage = "Log Out";
        }
        $this->songView->showList($albums, $songs, "Management", $message, $session, $sessionMessage); 
    }

    function insertComment(){
        $this->checkLoggedIn();
        $ranking = $_POST['input_ranking'];
        $comment = $_POST['input_comment'];
        $song_id = $_POST['song_id'];
        echo ($song_id);die();
        $user = $_SESSION['ID'];

        // verifico campos obligatorios
        if (empty($ranking) || empty($comment)) {
            $this->Home();
            die();
        }
        $id = $this->commentModel->addComment($ranking, $comment, $user, $song_id);
        $this->showSong($song_id);
    }
}