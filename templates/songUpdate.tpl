{include file="header.tpl"}

<!-- ======= Team Section ======= -->
<section id="team" class="team">
    <div class="container" data-aos="fade-up">
    <div class="section-title"><span class="text-danger">{$message}</span></div>
    <div class="section-title">
          <h2>Songs</h2>
          <p>Edit Song</p>
    </div>
    <div class="container">
        <form action="update_song_{$song->id}" method="POST">
            <div class="form-group">
                <label for="title">Title</label>
                <input class="form-control" name="title" aria-describedby="emailHelp" value="{$song->title}">
            </div>
            <div class="form-group">
                <label for="duration">Duration</label>
                <input class="form-control "name="duration" value="{$song->duration}">
            </div>
            <div class="form-group">
                <label for="writer">Writer</label>
                <input class="form-control "name="writer" value="{$song->writer}">
            </div>
            <div class="form-group">
                <label for="album">Album</label>
                <select name="album">
                    {foreach from=$albums item=album}
                        <option value="{$album->id}">{$album->name}</option>
                    {/foreach}
                </select> {$song->name}
            </div>
            <button type="submit" class="btn btn-primary">Edit Song</button>
        </form>
    </div>
</section>

{include file="footer.tpl"}