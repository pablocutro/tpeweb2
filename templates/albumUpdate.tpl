{include file="header.tpl"}

<!-- ======= Team Section ======= -->
<section id="team" class="team">
    <div class="container" data-aos="fade-up">
    <div class="section-title"><span class="text-danger">{$message}</span></div>
    <div class="section-title">
          <h2>Albums</h2>
          <p>Edit Album</p>
    </div>
    <div class="container">
        <form action="update_album_{$album->id}" method="POST">
            <div class="form-group">
                <label for="name">Name</label>
                <input class="form-control" name="name" aria-describedby="emailHelp" value="{$album->name}">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <input class="form-control "name="description" value="{$album->description}">
            </div>
            <div class="form-group">
                <label for="year">Year</label>
                <input class="form-control "name="year" value="{$album->year}">
            </div>
            <button type="submit" class="btn btn-primary">Edit Album</button>
        </form>
    </div>
</section>

{include file="footer.tpl"}