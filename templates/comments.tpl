{literal}
<div>
    <h3>Comments</h3>
    <div>

        <form action="insertComment" method="POST">
        <label name="song_id" value="{$song->id}"></label>
            <label>Ranking</label>
            <select name="input_ranking" required id="input_ranking">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
            <input type="text" name="input_comment" id="input_comment" placeholder="Add comment" required>
            <button type="submit" class="btn btn-primary">Comment</button>
        </form>

    </div>

    <ul>
        {foreach from=$comments item=comment}
            <li>{$comment->comment}<a href="">Delete</a></li>
        {/foreach}
    </ul>
    

    </div>
{/literal}