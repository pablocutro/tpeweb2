{include file="header.tpl"}

<!-- ======= Team Section ======= -->
<section id="team" class="team">
    <div class="container" data-aos="fade-up">
    <div class="section-title"><span class="text-danger">{$message}</span></div>
    <div class="section-title">
          <h2>Albums</h2>
          <p>Add Album</p>
    </div>
    <div class="container">
        <div class="form-group">
            <select>
                {foreach from=$albums item=album}
                    <option id="{$album->id}">{$album->name}</option>
                {/foreach}
            </select>
            <label for="album">Albums</label>
        </div>
        <form action="insert_album" method="POST">
            <div class="form-group">
                <label for="name">Name</label>
                <input class="form-control" name="name" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <input class="form-control "name="description">
            </div>
            <div class="form-group">
                <label for="year">Year</label>
                <input type="number" class="form-control" name="year">
            </div>
            <button type="submit" class="btn btn-primary">Add Album</button>
        </form>
    </div>
</section>

{include file="footer.tpl"}