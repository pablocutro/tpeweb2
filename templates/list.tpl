{include file="header.tpl"}

<!-- ======= Team Section ======= -->
<section id="team" class="team">
    <div class="container" data-aos="fade-up">
    <div class="section-title"><span class="text-danger">{$message}</span></div>
    <div class="section-title">
        <h2>Albums</h2>
    </div>
    <div class="section-title">
        <h5><a href="showInsert_album">Add Album</a></h5>
    </div>
    <div class="container">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Year</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delete</th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$albums item=album}
                    <tr>
                        <td>{$album->name}</td>
                        <td>{$album->description}</td>
                        <td>{$album->year}</td>
                        <td><a href="showUpdate_album_{$album->id}">Edit</td>
                        <td><a href="delete_album_{$album->id}">Delete</td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    </div>

<!-- ======= Team Section ======= -->
<section id="team" class="team">
    <div class="container" data-aos="fade-up">
    <div class="section-title">
        <h2>Songs</h2>
    </div>
    <div class="section-title">
        <h5><a href="showInsert_song">Add Song</a></h5>
    </div>
    <div class="container">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Title</th>
                    <th scope="col">Duration</th>
                    <th scope="col">Writer</th>
                    <th scope="col">Album</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Delete</th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$songs item=song}
                    <tr>
                        <td>{$song->title}</td>
                        <td>{$song->duration}</td>
                        <td>{$song->writer}</td>
                        <td>{$song->name}</td>
                        <td><a href="showUpdate_song_{$song->id}">Edit</td>
                        <td><a href="delete_song_{$song->id}">Delete</td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
    </div>

{include file="footer.tpl"}