{include file="header.tpl"}

<!-- ======= Team Section ======= -->
<section id="team" class="team">
    <div class="container" data-aos="fade-up">
    <div class="section-title"><span class="text-danger">{$message}</span></div>
    <div class="section-title">
          <h2>Songs</h2>
          <p>Add Song</p>
    </div>
    <div class="container">
        <div class="form-group">
            <select>
                {foreach from=$songs item=song}
                    <option>{$song->title}</option>
                {/foreach}
            </select>
            <label for="song">Songs</label>
        </div>
        <form action="insert_song" method="POST">
            <div class="form-group">
                <label for="title">Title</label>
                <input class="form-control" name="title" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label for="duration">Duration</label>
                <input class="form-control "name="duration">
            </div>
            <div class="form-group">
                <label for="writer">Writer</label>
                <input class="form-control "name="writer">
            </div>
            <div class="form-group">
                <label for="album">Album</label>
                <select name="album">
                    {foreach from=$albums item=album}
                        <option value="{$album->id}">{$album->name}</option>
                    {/foreach}
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Add Song</button>
        </form>
    </div>
</section>

{include file="footer.tpl"}