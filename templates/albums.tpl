{include file="header.tpl"}

  <main id="main">
    
    <!-- ======= Team Section ======= -->
    <section id="team" class="team">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Albums</h2>
          <p>All Albums</p>
        </div>

        <div class="row">

          {foreach from=$albums item=album}
              
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
              <div class="member" data-aos="fade-up" data-aos-delay="100">
                <div class="member-img">
                  <a href="album_{$album->name}">
                    <img src="assets/img/albums/{$album->name}.jpg" class="img-fluid" alt="">
                  </a>
                </div>
                <div class="member-info">
                  <h3>{$album->name}</h3>
                  <span>{$album->year}</span>
                  <span>{$album->description}</span>
                </div>
              </div>
            </div>

          {/foreach}
          

          

          

        </div>

      </div>
    </section><!-- End Team Section -->

{include file="footer.tpl"}