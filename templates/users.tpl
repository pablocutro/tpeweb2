<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Users</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/loginStyle.css">
</head>
<body>
        
        <!-- ======= Team Section ======= -->
        <section id="team" class="team">
            <div class="container" data-aos="fade-up">
                <a href="home">Home</a>
            </div>
            <div class="container" data-aos="fade-up">
            <div class="section-title">
                <h2>Users</h2>
                <span class="text-danger">{$message}</span>
            </div>
            <div class="container">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th>Mail</th>
                            <th>User Role</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach from=$users item=user}
                            <tr>
                                <td>{$user->email}</td>
                                <td>
                                    <form method="POST" action="editUser_{$user->id}">
                                        <select name="role">
                                        <option value="0" selected>User</option>
                                        <option value="1" {if $user->admin eq 1} selected {/if}>Admin</option>
                                        </select>
                                        <button>Edit</button>
                                    </form>
                                </td>
                                <td>
                                    <a href="deleteUser_{$user->id}"><i class="far fa-trash-alt "></i>X</a>
                                </td>
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
            </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
</body>

</html>
