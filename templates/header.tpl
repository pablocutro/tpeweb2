<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>{$title}</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center justify-content-between">

      <a href="home" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="home">Home</a></li>
          <li class="drop-down"><a href="">Songs</a>
            <ul>
              <li><a href="songs">All Songs</a></li>

              {foreach from=$albums item=album}
                <li><a href="songs_{$album->name}">{$album->name}</a></li>
              {/foreach}

            </ul>
		      <li class="drop-down"><a href="">Albums</a>
            <ul>
                <li><a href="albums">All Albums</a></li>
                
                  {foreach from=$albums item=album}
                    <li><a href="album_{$album->name}">{$album->name}</a></li>
                  {/foreach}

            </ul>
          </li>
          <li><a href="list">Management</a><li>
          <li><a href="users">Users</a><li>
          <li><a href="register">Register</a></li>
        </ul>
      </nav><!-- .nav-menu -->

      <a href="{$session}" class="get-started-btn scrollto">{$sessionMessage}</a>

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center justify-content-center">
    <div class="container" data-aos="fade-up">

      <div class="row justify-content-center" data-aos="fade-up" data-aos-delay="150">
        <div class="col-xl-6 col-lg-8">
          <h1>Two Steps From Hell</h1>
          <h2>An American production music company</h2>
        </div>
      </div>   
    </div>  
  </section><!-- End Hero -->