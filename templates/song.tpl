{include file="header.tpl"}

  <main id="main">

    <section id="team" class="team">
      <!-- ======= About Section ======= -->
      <section id="about" class="about">
        <div class="container" data-aos="fade-up">

          <div class="row">
            <div class="col-lg-6 order-1 order-lg-2" data-aos="fade-left" data-aos-delay="100">
              <img src="assets/img/albums/{$song->name}.jpg" class="img-fluid" alt="">
            </div>
            <div class="col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content" data-aos="fade-right" data-aos-delay="100">
              <h3>{$song->title}</h3>
              <p class="font-italic">
                {$song->duration}
              </p>
              <p>
                {$song->writer}
              </p>
              <p>
                Album: {$song->name}
              </p>
            </div>
          </div>

        </div>
      </section><!-- End About Section -->
    </section>


    <!-- ======= Team Section ======= -->
    <section id="team" class="team">
    <div class="container" data-aos="fade-up">
    <div class="container">
        <div class="form-group">
        {foreach from=$comments item=comment}
            <li>{$comment->comment}</li>
        {/foreach}
      {include file="templates/comments.tpl"}
    </section>

<script src="./assets/comments.js"></script>    
{include file="footer.tpl"}