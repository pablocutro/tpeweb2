{include file="header.tpl"}

  <main id="main">
    
    <!-- ======= Team Section ======= -->
    <section id="team" class="team">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Songs</h2>
          <p>{$album}</p>
        </div>

        <div class="row">

          {foreach from=$songs item=song}
              
            <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
              <div class="member" data-aos="fade-up" data-aos-delay="100">
                <div class="member-img">
                  <a href="song_{$song->id}">
                    <img src="assets/img/albums/{$song->name}.jpg" class="img-fluid" alt="">
                  </a>
                </div>
                <div class="member-info">
                  <h3>{$song->title}</h3>
                  <span>{$song->duration}</span>
                  <span>{$song->writer}</spa n>
                  <span>{$song->year}</span>
                  <h6>{$song->name}</h6>
                </div>
              </div>
            </div>

          {/foreach}
          

          

          

        </div>

      </div>
    </section><!-- End Team Section -->

{include file="footer.tpl"}