-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-10-2020 a las 01:26:59
-- Versión del servidor: 10.1.39-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_songs`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `album`
--

CREATE TABLE `album` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `description` varchar(255) NOT NULL,
  `year` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `album`
--

INSERT INTO `album` (`id`, `name`, `description`, `year`) VALUES
(1, 'Invincible', 'Compilation of some of the most popular Two Steps from Hell tracks', 2010),
(2, 'Archangel', 'Compilation of some of the most popular Two Steps from Hell tracks', 2011),
(3, 'Illusions', 'Thomas Bergersen solo album, formerly titled Nemesis II', 2011),
(4, 'Halloween', 'Compilation of well-known horror tracks from several demonstration albums, with several other-genre tracks as well', 2012),
(5, 'SkyWorld', 'First public album to use mostly all-new tracks', 2012),
(6, 'Classics Volume One', 'Compilation of previously unreleased tracks in the same vein as Invincible and Archangel. First public album available in lossless audio', 2013),
(7, 'Orion', 'Short epic hybrid album composed by Michal Cielecki', 2013),
(8, 'Miracles', 'An epic drama/emotional album with the best of Illumina, Dreams & Imaginations, Two Steps From Heaven, and a few new tracks composed by Thomas Bergersen', 2014),
(9, 'Battlecry', 'Public Album; follow-up to SkyWorld and the first public album to be released on two discs. It was later re-released in 2017 as Battlecry Anthology, featuring instrumental and orchestral versions of all the tracks from the original album', 2015),
(10, 'Classics Volume Two', 'Compilation of new and previously unreleased tracks', 2015),
(11, 'Vanquish', 'Features vocal performances by Felicia Farerre, Asja Kadric, Jenifer Thigpen, and Linea Adamson', 2016),
(12, 'Unleashed', 'Features vocal performances by Merethe Soltvedt, C.C. White, Felicia Farerre, Uyanga Bold, Nick Phoenix and Linea Adamson', 2017),
(13, 'Dragon', 'Features vocal performances by Felicia Farerre, Merethe Soltvedt, and Uyanga Bold', 2019),
(15, 'Prueba2', 'Probando Update', 2020);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `song`
--

CREATE TABLE `song` (
  `id` int(11) NOT NULL,
  `title` varchar(40) NOT NULL,
  `duration` varchar(10) NOT NULL,
  `writer` varchar(40) NOT NULL,
  `id_album` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `song`
--

INSERT INTO `song` (`id`, `title`, `duration`, `writer`, `id_album`) VALUES
(1, 'Invincible', '2:52', 'Thomas J. Bergersen', 1),
(2, 'Heart of Courage', '1:57', 'Thomas J. Bergersen', 1),
(3, 'Freedom Fighters', '2:31', 'Thomas J. Bergersen', 1),
(4, 'Moving Mountains', '3:02', 'Thomas J. Bergersen', 1),
(5, 'Fire Nation', '3:00', 'Nick Phoenix', 1),
(6, 'Protectors of the Earth', '2:50', 'Thomas J. Bergersen', 1),
(7, 'Tristan', '2:33', 'Thomas J. Bergersen', 1),
(8, 'To Glory', '4:36', 'Thomas J. Bergersen', 1),
(9, 'Archangel', '2:35', 'Thomas J. Bergersen', 2),
(10, 'Mercy in Darkness', '1:11', 'Nick Phoenix', 2),
(11, 'The Last Stand', '2:50', 'Nick Phoenix', 2),
(12, 'Nero', '3:27', 'Thomas J. Bergersen', 2),
(13, 'Strength of a Thousand Men', '2:17', 'Thomas J. Bergersen', 2),
(14, 'Dragon Rider', '1:54', 'Thomas J. Bergersen', 2),
(15, 'Army of Justice', '2:04', 'Thomas J. Bergersen', 2),
(16, 'Aesir', '4:50', 'Nick Phoenix', 2),
(17, 'Power of Darkness', '2:51', 'Thomas J. Bergersen', 4),
(18, 'Moving Shadows', '1:40', 'Thomas J. Bergersen', 4),
(19, 'Take Me to Hell', '2:17', 'Nick Phoenix', 4),
(20, 'Shed My Skin', '3:47', 'Nick Phoenix', 4),
(21, 'She Rises', '1:23', 'Thomas J. Bergersen', 4),
(22, 'Calamity', '1:41', 'Thomas J. Bergersen', 4),
(23, 'It Lives', '1:36', 'Thomas J. Bergersen', 4),
(24, 'Rising Darkness', '1:50', 'Nick Phoenix', 4),
(25, 'Black Assassin', '2:21', 'Thomas J. Bergersen', 4),
(26, 'Otherworld', '2:53', 'Nick Phoenix', 4),
(27, 'Illusions', '8:02', 'Thomas J. Bergersen', 3),
(28, 'Aura', '7:43', 'Thomas J. Bergersen', 3),
(29, 'Starvation', '4:27', 'Thomas J. Bergersen', 3),
(30, 'Dreammaker', '4:18', 'Thomas J. Bergersen', 3),
(31, 'Ocean Princess', '2:53', 'Thomas J. Bergersen', 3),
(32, 'A Place in Heaven', '4:16', 'Thomas J. Bergersen', 3),
(33, 'Homecoming', '2:57', 'Thomas J. Bergersen', 3),
(34, 'Immortal', '4:08', 'Thomas J. Bergersen', 3),
(35, 'Reborn', '3:53', 'Thomas J. Bergersen', 3),
(36, 'Age of Gods', '2:30', 'Thomas J. Bergersen', 3),
(37, 'Soulseeker', '3:15', 'Thomas J. Bergersen', 3),
(38, 'SkyWorld', '3:12', 'Thomas J. Bergersen', 5),
(39, 'Titan Dream', '4:03', 'Nick Phoenix', 5),
(40, 'El Dorado', '4:14', 'Thomas J. Bergersen', 5),
(41, 'Blackheart', '4:26', 'Thomas J. Bergersen', 5),
(42, 'Juggernaut', '2:34', 'Nick Phoenix', 5),
(43, 'Dark Ages', '3:25', 'Thomas J. Bergersen', 5),
(44, 'Queen of Crows', '2:43', 'Nick Phoenix', 5),
(45, 'Blizzard', '2:53', 'Thomas J. Bergersen', 5),
(46, 'Back to the Earth', '4:24', 'Nick Phoenix', 5),
(47, 'Ocean Kingdom', '2:20', 'Nick Phoenix', 5),
(48, 'Armada', '3:06', 'Thomas J. Bergersen', 6),
(49, 'Fall of the Fountain World', '4:39', 'Nick Phoenix', 11),
(50, 'Nemesis', '1:21', 'Thomas J. Bergersen', 6),
(51, 'Sons of War', '1:59', 'Thomas J. Bergersen', 6),
(52, 'Return from Darkness', '3:36', 'Nick Phoenix', 6),
(53, 'Strength of an Empire', '3:53', 'Thomas J. Bergersen', 6),
(54, 'Eternal Sorrow', '2:22', 'Thomas J. Bergersen', 6),
(55, 'The Ancients', '2:59', 'Nick Phoenix', 6),
(56, 'Birth of a Hero', '2:15', 'Thomas J. Bergersen', 6),
(57, 'Sky Titans', '2:09', 'Thomas J. Bergersen', 6),
(58, 'Earth Rising', '3:23', 'Thomas J. Bergersen', 6),
(59, 'Ironheart', '3:14', 'Thomas J. Bergersen', 6),
(60, 'White Witch', '2:44', 'Nick Phoenix', 6),
(61, 'Stormwatch', '2:22', 'Nick Phoenix', 6),
(62, 'Orion', '8:08', 'Michal Chielecki', 7),
(63, 'Aeterna', '4:00', 'Michal Chielecki', 7),
(64, 'The Fire in Her Eyes', '5:33', 'Michal Chielecki', 7),
(65, 'Rain of Light', '3:49', 'Michal Chielecki', 7),
(66, 'Rebirth', '5:52', 'Michal Chielecki', 7),
(67, 'Miracles', '5:30', 'Thomas J. Bergersen', 8),
(68, 'Sun Gazer', '2:56', 'Thomas J. Bergersen', 8),
(69, 'Stay', '3:22', 'Thomas J. Bergersen, Merethe Soltvedt', 8),
(70, 'Fountain of Life', '3:29', 'Thomas J. Bergersen', 8),
(71, 'Men of Honor', '3:20', 'Thomas J. Bergersen', 8),
(72, 'Perfect Love', '3:30', 'Thomas J. Bergersen', 8),
(73, 'Lux Aeterna', '3:26', 'Thomas J. Bergersen', 8),
(74, 'I Love You Forever', '4:00', 'Thomas J. Bergersen', 8),
(75, 'Breath of Cold Air', '4:24', 'Thomas J. Bergersen', 8),
(76, 'Hearth', '3:00', 'Thomas J. Bergersen', 8),
(77, 'Wind Queen', '2:06', 'Thomas J. Bergersen', 8),
(78, 'Lost in Las Vegas', '10:15', 'Thomas J. Bergersen', 8),
(79, 'Victory', '5:20', 'Thomas J. Bergersen', 9),
(80, 'None Shall Live', '2:20', 'Thomas J. Bergersen', 9),
(81, 'Stormkeeper', '2:55', 'Nick Phoenix', 9),
(82, 'Spellcaster', '3:02', 'Nick Phoenix', 9),
(83, 'Never Back Down', '2:56', 'Thomas J. Bergersen', 9),
(84, 'Red Tower', '3:09', 'Nick Phoenix', 9),
(85, 'Blackout', '3:46', 'Nick Phoenix', 9),
(86, 'Battleborne', '4:59', 'Nick Phoenix', 9),
(87, 'Star Sky', '5:31', 'Thomas J. Bergersen', 9),
(88, 'Unforgiven', '4:10', 'Thomas J. Bergersen', 9),
(89, 'Heaven & Earth', '2:18', 'Thomas J. Bergersen', 10),
(90, 'Submariner', '2:52', 'Nick Phoenix', 10),
(91, 'Neverdark', '3:09', 'Nick Phoenix', 10),
(92, 'Starfall', '3:18', 'Thomas J. Bergersen', 10),
(93, 'Mythic', '2:58', 'Nick Phoenix', 10),
(94, 'Magika', '1:57', 'Thomas J. Bergersen', 10),
(95, 'The Immortals', '2:10', 'Thomas J. Bergersen', 10),
(96, 'Ride to Victory', '2:00', 'Thomas J. Bergersen', 10),
(97, 'The Colonel', '2:31', 'Thomas J. Bergersen', 10),
(98, 'Dachuur', '3:18', 'Nick Phoenix', 10),
(99, 'Undefeated', '2:22', 'Thomas J. Bergersen', 10),
(100, 'Adventure of a Lifetime', '1:09', 'Thomas J. Bergersen', 10),
(101, 'Vanquish', '3:15', 'Nick Phoenix', 11),
(102, 'Pegasus', '4:29', 'Thomas J. Bergersen', 11),
(103, 'Future Guardian', '4:14', 'Nick Phoenix', 11),
(104, 'Enchantress', '8:17', 'Thomas J. Bergersen', 11),
(105, 'Final Kingdom', '2:33', 'Nick Phoenix', 11),
(106, 'Evergreen', '3:02', 'Thomas J. Bergersen', 11),
(107, 'Forge', '4:11', 'Nick Phoenix', 11),
(108, 'Dangerous', '3:56', 'Thomas J. Bergersen', 11),
(109, 'Unleashed', '5:25', 'Thomas J. Bergersen, Merethe Soltvedt', 12),
(110, 'Impossible', '8:55', 'Thomas J. Bergersen, Merethe Soltvedt', 12),
(111, 'Wild Heart', '3:39', 'Thomas J. Bergersen', 12),
(112, 'Run Free', '2:37', 'Thomas J. Bergersen', 12),
(113, 'Step into the Light', '4:51', 'Nick Phoenix', 12),
(114, 'Secret Melody', '3:47', 'Thomas J. Bergersen', 12),
(115, 'Snow Angels', '2:46', 'Thomas J. Bergersen', 12),
(116, 'I\'ll Stand Alone', '4:21', 'Nick Phoenix', 12),
(117, 'Emblem', '3:40', 'Nick Phoenix', 12),
(118, 'Dragon', '3:44', 'Nick Phoenix', 13),
(119, 'Unbreakable', '4:17', 'Thomas J. Bergersen', 13),
(120, 'Bravestone', '3:18', 'Thomas J. Bergersen', 13),
(121, 'Emerald Princess', '11:02', 'Thomas J. Bergersen', 13),
(122, 'Dragonwing', '2:57', 'Thomas J. Bergersen', 13),
(123, 'Riders Of The Apocalypse', '2:52', 'Nick Phoenix', 13),
(124, 'Letters to God', '3:17', 'Thomas J. Bergersen', 13),
(125, 'Nighthawk', '3:42', 'Nick Phoenix', 13),
(126, 'Cathedral', '3:57', 'Nick Phoenix', 13),
(127, 'Believe', '4:53', 'Thomas J. Bergersen', 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `email`, `password`) VALUES
(1, 'pablo.cutro@gmail.com', '$2y$10$2XH6/PIdcazDCiSmo5iULue1S3di7uFPx.N15miZwvGXpJbiVW7Q6'),
(2, 'prueba@gmail.com', '$2y$10$lqm.rip/OQUm.AYN1c92y.ty8xuvMBTcx.0TOC4oBkKZffX2CFD0u');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `song`
--
ALTER TABLE `song`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_album` (`id_album`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `album`
--
ALTER TABLE `album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `song`
--
ALTER TABLE `song`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `song`
--
ALTER TABLE `song`
  ADD CONSTRAINT `song_ibfk_1` FOREIGN KEY (`id_album`) REFERENCES `album` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
