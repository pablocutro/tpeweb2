<?php
    require_once "./Model/CommentModel.php";
    require_once "api/ApiController.php";

    class CommentApiController extends ApiController{

        function __construct(){
            parent::__construct();
            $this->model = new CommentModel();
        }

        function getComments($params = null){
            $id = $params[':ID'];
            $comments= $this->model->getComments($id);

            if($comments)
                $this->view->response($comments, 200);
            else
                $this->view->response($comments, 404);
        }

        function deleteComment($params = null){
            $id= $params[":ID"];
            $comment= $this->model->getComment($id);

            if($comment){
                $this->model->deleteComment($id);
                $this->view->response($comment, 200);
            }
            else
                $this->view->response("The comment with id = $id does not existe", 404);
        }

        function addComment(){
            $request = $this->getRequest();

            $id = $this->model->addComment($request->puntuacion, $request->comment, $request->id_user, $request->id_song);
            $comment = $this->model->getComment($id);

            if($comment)
                $this->view->response($comment, 200);
            else
                $this->view->response("The comment cannot be inserted", 404);

        }
    }