<?php

class CommentModel{

    private $db;

    public function __construct(){
        $this->db= new PDO('mysql:host=localhost;'.'dbname=db_songs;charset=utf8', 'root', '');
    }
        
   function getComments ($id_song) {
       $query = $this->db->prepare("SELECT users.email AS email, comments.* FROM comments JOIN users ON users.id = comments.id_user WHERE comments.id_song = ?");
       $query->execute(array($id_song));
       return $query->fetchAll(PDO::FETCH_OBJ);
   }

   function getComment ($id) {
        $query = $this->db->prepare("SELECT users.email AS email, comments.* FROM comments JOIN users ON comments.id_user = users.id WHERE comments.id = ?");
        $query->execute(array($id));
        return $query->fetch(PDO::FETCH_OBJ);
   }

   function deleteComment ($id) {
       $query = $this->db->prepare("DELETE FROM comments WHERE id = ?");
       $query->execute(array($id));
   }

   function addComment ($ranking, $comment, $id_user, $id_song) {
       $query = $this->db->prepare("INSERT INTO comments (ranking, comment, id_user, id_song) VALUES (? ,? ,? ,?)");
       $query->execute(array($ranking, $comment, $id_user, $id_song));
       return $this->db->lastInsertId();
   }

}