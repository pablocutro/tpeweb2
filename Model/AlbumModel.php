<?php

class AlbumModel {

    private $db;

    public function __construct() {
        $this->db = new PDO('mysql:host=localhost;'.'dbname=db_songs;charset=utf8', 'root', '');
    }

    /**
     *  Obtiene la lista de albumes
     */
    function getAlbum($album) {
        $query = $this->db->prepare('SELECT * FROM ALBUM WHERE name = ?');
        $query->execute([$album]); // array($album)
        $albums = $query->fetchAll(PDO::FETCH_OBJ);
        return $albums;
    }

    function getAlbumById($id) {
        $query = $this->db->prepare('SELECT * FROM ALBUM WHERE id = ?');
        $query->execute([$id]); // array($album)
        $albums = $query->fetchAll(PDO::FETCH_OBJ);
        return $albums;
    }

    /**
     * Obtiene todos los albums
     */
    function getAlbums() {
        $query = $this->db->prepare('SELECT * FROM ALBUM');
        $query->execute();
        $albums = $query->fetchAll(PDO::FETCH_OBJ);
        return $albums;
    }

    function insert($name, $description, $year) {

        $query = $this->db->prepare('INSERT INTO album (name, description, year) VALUES (?,?,?)');
        $query->execute([$name, $description, $year]);

        return $this->db->lastInsertId();
    }

    function delete($id) {  
        $query = $this->db->prepare('DELETE FROM album WHERE id = ?');
        $query->execute([$id]);
    }

    function update($id, $name, $description, $year) {
        $query = $this->db->prepare('UPDATE album SET name = ?, description = ?, year = ? WHERE id = ?');
        $result = $query->execute([$name, $description, $year, $id]);
    }
}