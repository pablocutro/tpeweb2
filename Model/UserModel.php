<?php

class UserModel {
    private $db;

    function __construct() {
        $this->db = $this->connect();
    }

    private function connect() {
        $db = new PDO('mysql:host=localhost;'.'dbname=db_songs;charset=utf8', 'root', '');
        return $db;
    }

    /**
     * Devuelve un usuario dado un email.
     */
    public function getByEmail($email) {
        $query = $this->db->prepare('SELECT * FROM users WHERE email = ?');
        $query->execute([$email]);
        return $query->fetch(PDO::FETCH_OBJ);
    }

    public function register($email, $password){
        $hash_password = password_hash($password, PASSWORD_DEFAULT);

        $query = $this->db->prepare('INSERT INTO users (email, password) VALUES (?,?)');
        $query->execute([$email, $hash_password]);
        return $this->db->lastInsertId();
    }

    function getUsers () {
        $query = $this->db->prepare("SELECT * FROM users");
        $query->execute();
        return $query->fetchAll(PDO::FETCH_OBJ);
    }

    public function getUserById($id) {
        $query = $this->db->prepare('SELECT * FROM users WHERE id = ?');
        $query->execute([$id]);
        return $query->fetch(PDO::FETCH_OBJ);
    }

    function deleteUser($id){
        $query = $this->db->prepare("DELETE FROM users WHERE id = ?");
        $query->execute(array($id));
    }

    function updateUserRole ($id, $role){
        $query = $this->db->prepare("UPDATE users SET admin = ? WHERE id = ?");
        $query->execute(array($role, $id));
    }

}