<?php

class SongModel {

    private $db;

    public function __construct() {
        $this->db = new PDO('mysql:host=localhost;'.'dbname=db_songs;charset=utf8', 'root', '');
    }

    /**
     *  Obtiene la lista de canciones de la DB según álbum
     */
    function getSongsByAlbum($album) {
        $query = $this->db->prepare('SELECT s.id, s.title, s.duration, s.writer, a.year, a.name FROM SONG s INNER JOIN ALBUM a ON s.id_album = a.id WHERE a.name = ?');
        $query->execute([$album]); // array($album)
        $songs = $query->fetchAll(PDO::FETCH_OBJ);
        return $songs;
    }

    function getSongsByAlbumId($album_id) {
        $query = $this->db->prepare('SELECT s.id, s.title, s.duration, s.writer, a.year, a.name FROM SONG s INNER JOIN ALBUM a ON s.id_album = a.id WHERE a.id = ?');
        $query->execute([$album_id]); // array($album)
        $songs = $query->fetchAll(PDO::FETCH_OBJ);
        return $songs;
    }

    /**
     * Obtiene todas las peliculas
     */
    function getSongs() {
        $query = $this->db->prepare('SELECT s.id, s.title, s.duration, s.writer, a.year, a.name FROM SONG s INNER JOIN ALBUM a ON s.id_album = a.id');
        $query->execute();
        $songs = $query->fetchAll(PDO::FETCH_OBJ);
        return $songs;
    }

    function getSong($id) {
        $query = $this->db->prepare('SELECT s.id, s.title, s.duration, s.writer, a.year, a.name FROM SONG s INNER JOIN ALBUM a ON s.id_album = a.id WHERE s.id = ?');
        $query->execute([$id]);
        $songs = $query->fetchAll(PDO::FETCH_OBJ);
        return $songs;
    }

    function insert($title, $duration, $writer, $album_id){

        // 2. Enviar la consulta (2 sub-pasos: prepare y execute)
        $query = $this->db->prepare('INSERT INTO song (title, duration, writer, id_album) VALUES (?,?,?,?)');
        $query->execute([$title, $duration, $writer, $album_id]);

        // 3. Obtengo y devuelo el ID de la tarea nueva
        return $this->db->lastInsertId();
    }

    function delete($id) {  
        $query = $this->db->prepare('DELETE FROM song WHERE id = ?');
        $query->execute([$id]);
    }

    function update($id, $title, $duration, $writer, $album_id) {
        $query = $this->db->prepare('UPDATE song SET title = ?, duration = ?, writer = ?, id_album = ? WHERE id = ?');
        $result = $query->execute([$title, $duration, $writer, $album_id, $id]);
    }
}