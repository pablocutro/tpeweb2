<?php
require_once ('api/CommentApiController.php');
require_once 'RouterClass.php';

$router = new Router();

    $router->addRoute("comments/:ID", "GET", "CommentApiController", "getComments");
    $router->addRoute("comments", "POST", "CommentApiController", "addComment");
    $router->addRoute("comments/:ID", "DELETE", "CommentApiController", "deleteComment");

    
    $router->route($_GET['resource'], $_SERVER['REQUEST_METHOD']); 
?>