<?php
require_once('libs/smarty/Smarty.class.php');

class UserView {

    function showFormLogin($message) {
        $smarty = new Smarty();
        $smarty->assign('message', $message);
        $smarty->display('templates/login.tpl');
    }

    function showRegister($message) {
        $smarty = new Smarty();
        $smarty->assign('message', $message);
        $smarty->display('templates/register.tpl');
    }    

    function showUsers ($users, $message) {
        $smarty = new Smarty();
        $smarty->assign('users', $users);
        $smarty->assign('message', $message);
        $smarty->display('templates/users.tpl');
    }

}