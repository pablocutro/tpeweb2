<?php

require_once "./libs/smarty/Smarty.class.php";

class SongView {

    function __construct(){
        
    }

    function ShowHome($albums, $title, $session, $sessionMessage){
        $smarty = new Smarty();
        $smarty->assign('title', $title);
        $smarty->assign('albums', $albums);
        $smarty->assign('session', $session);
        $smarty->assign('sessionMessage', $sessionMessage);
        $smarty->display('templates/home.tpl'); // muestro el template 
    }
    
    function renderSongsByAlbum($album, $songs, $albums, $title, $session, $sessionMessage) {
        $smarty = new Smarty();
        $smarty->assign('title', $title);
        $smarty->assign('songs', $songs);
        $smarty->assign('album', $album);
        $smarty->assign('albums', $albums);
        $smarty->assign('session', $session);
        $smarty->assign('sessionMessage', $sessionMessage);
        $smarty->display('templates/songs.tpl');
    }

    function renderSongs($songs, $albums, $album, $title, $session, $sessionMessage) {
        $smarty = new Smarty();
        $smarty->assign('title', $title);
        $smarty->assign('songs', $songs);
        $smarty->assign('album', $album);
        $smarty->assign('albums', $albums);
        $smarty->assign('session', $session);
        $smarty->assign('sessionMessage', $sessionMessage);
        $smarty->display('templates/songs.tpl');
    }

    function renderSong($albums, $song, $title, $session, $sessionMessage, $user, $comments) {
        $smarty = new Smarty();
        $smarty->assign('title', $title);
        $smarty->assign('song', $song);
        $smarty->assign('albums', $albums);
        $smarty->assign('session', $session);
        $smarty->assign('sessionMessage', $sessionMessage);
        $smarty->assign('user', $user);
        $smarty->assign('comments', $comments);
        $smarty->display('templates/song.tpl');
    }

    function showInsert($albums, $songs, $title, $message, $session, $sessionMessage){
        $smarty = new Smarty();
        $smarty->assign('title', $title);
        $smarty->assign('songs', $songs);
        $smarty->assign('albums', $albums);
        $smarty->assign('message', $message);
        $smarty->assign('session', $session);
        $smarty->assign('sessionMessage', $sessionMessage);
        $smarty->display('templates/insertSong.tpl');
    } 

    function showUpdate($albums, $songs, $song, $title, $message, $session, $sessionMessage){
        $smarty = new Smarty();
        $smarty->assign('title', $title);
        $smarty->assign('songs', $songs);
        $smarty->assign('albums', $albums);
        $smarty->assign('song', $song);
        $smarty->assign('message', $message);
        $smarty->assign('session', $session);
        $smarty->assign('sessionMessage', $sessionMessage);
        $smarty->display('templates/songUpdate.tpl');
    }

    function showList($albums, $songs, $title, $message, $session, $sessionMessage){
        $smarty = new Smarty();
        $smarty->assign('title', $title);
        $smarty->assign('songs', $songs);
        $smarty->assign('albums', $albums);
        $smarty->assign('message', $message);
        $smarty->assign('session', $session);
        $smarty->assign('sessionMessage', $sessionMessage);
        $smarty->display('templates/list.tpl');
    } 

}