<?php

class AlbumView {
    
    function renderAlbums($albums, $album, $title, $session, $sessionMessage) {
        $smarty = new Smarty();
        $smarty->assign('title', $title);
        $smarty->assign('albums', $albums);
        $smarty->assign('session', $session);
        $smarty->assign('sessionMessage', $sessionMessage);
        $smarty->display('templates/albums.tpl');
    }

    function renderAlbum($albums, $album, $title, $session, $sessionMessage) {
        $smarty = new Smarty();
        $smarty->assign('title', $title);
        $smarty->assign('album', $album[0]);
        $smarty->assign('albums', $albums);
        $smarty->assign('session', $session);
        $smarty->assign('sessionMessage', $sessionMessage);
        $smarty->display('templates/album.tpl');
    }

    function showUpdate($albums, $album, $title, $message, $session, $sessionMessage){
        $smarty = new Smarty();
        $smarty->assign('title', $title);
        $smarty->assign('album', $album);
        $smarty->assign('albums', $albums);
        $smarty->assign('message', $message);
        $smarty->assign('session', $session);
        $smarty->assign('sessionMessage', $sessionMessage);
        $smarty->display('templates/albumUpdate.tpl');
    }

    function showInsert($albums, $title, $message, $session, $sessionMessage){
        $smarty = new Smarty();
        $smarty->assign('title', $title);
        $smarty->assign('albums', $albums);
        $smarty->assign('message', $message);
        $smarty->assign('session', $session);
        $smarty->assign('sessionMessage', $sessionMessage);
        $smarty->display('templates/insertAlbum.tpl');
    } 

}