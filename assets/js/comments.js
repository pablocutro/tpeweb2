document.addEventListener("DOMContentLoaded", function(){
    
    getComentarios();

    let usuario= document.getElementById("user").value;
    
    if(usuario != "null"){
        document.getElementById("formulario_coment").addEventListener("submit", function(e){
            e.preventDefault();
            insertComment();
        });
            
    }
    
});    

function getComments(){
    let ID= document.getElementById("id_song").value;
    const URL= "api/"; 
    const recurso= "/comments";

    fetch(URL+recurso+ID)
    .then(response => response.json())
    .then(comments=> renderComments(comments))
    .catch(error =>console.log(error));
}

function getComment(){
    let ID= document.getElementById("id_song").value;
    
    const URL= "api/comments/";

    fetch(URL+ID)
    .then(response => response.json())
    .then(comments=> renderComments(comments))
    .catch(error =>console.log(error));
}

function insertComment(){
    let comment={
        comment: document.getElementById("input_comentario").value,
        ranking: document.getElementById("input_ranking").value,
        id_user: document.getElementById("input_user").value,
        id_song: document.getElementById("id_song").value

    }

    console.log("fetch");
    fetch('api/comments', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(comment)
    }).then(response => {
        response.json()
        getComments();
    })
    .catch(error =>console.log(error));

}

function deleteComment(id){
    let parte= id;
    const URL="api/comments/";
    
    fetch(URL+parte, {
        method: 'DELETE',
        headers: {'Content-Type': 'application/json'}
    })
    .then(response => {
        response.json()
        getComments();
    })
    .catch(error =>console.log(error));
    
}


function renderComments(comen) {
    let lista= document.getElementById("list_comentarios");
    lista.innerHTML ="";

    let usuario= document.getElementById("usuario").value;

    comen.forEach(com => {
        let comentario=`<div class="small_row"><li>${com.comentario}</li>`;
        let button= `<button name="boton_borrar" onclick="deleteComentario(${com.id})" id="id_boton${com.id}">borrar</button>`;
        let puntuacion= `${com.puntuacion}`;
        
        if(usuario != "null"){
            tipo_usuario= document.getElementById("input_tipo").value;
            if ( tipo_usuario==1) {
                lista.innerHTML+= comentario +"- Puntuacion dada: "+ puntuacion + button +`</div>`;
            }if (tipo_usuario==0) {
                lista.innerHTML+= comentario +"- Puntuacion dada: "+ puntuacion +`</div>`;
            }
        }else{
            lista.innerHTML+= comentario +"- Puntuacion dada: "+ puntuacion +`</div>`;
        } 
    });
}